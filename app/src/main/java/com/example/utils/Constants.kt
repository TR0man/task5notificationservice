package com.example.utils

class Constants {

    companion object {
        // PRESENTER constants
        const val MESSAGE_TEXT = "message"
        const val TIME_MINUTES = "minutes"
        const val TIME_SECONDS = "seconds"
        const val ACTION_START = "start"
        const val DEFAULT_NOTIFICATION_TIME = 20
        const val EXTRAS_DEFAULT_VALUE = 0
        const val WRONG_TIME_PARAMETER = "0"
        const val DEFAULT_REQUEST_CODE = 1
        const val DEFAULT_FLAG_STATE = 0
        const val ZERO = 0

        val VIBRATE_PATTERN = longArrayOf(0, 500, 200, 500, 200, 500)

        // Notification Service constants
        const val ACTION_NEW_ALARM = "new message"
        const val ACTION_ONE_TIME_ALARM = "one time"
        const val ACTION_STOP = "ACTION_STOP"
        const val ACTION_CHANGE = "ACTION_CHANGE"
        const val ACTION_RESTART = "ACTION_RESTART"
        const val ACTION_REBOOT = "ACTION_REBOOT"
        const val ACTION_ONE_TIME = "one"
        const val ACTION_REPLY = "ACTION_REPLY"
        const val GROUP_KEY = "1"
        const val GROUP_ID = -10
        const val REPLY_ID = "reply id"
        const val REPLY_NOTIFICATION_ID = -20
        const val REPLY_SLEEP_TIME = 1L
        const val CHANEL_ID = "notification id"
        const val CHANEL_NAME = "Notification App Chanel"
        const val FOREGROUND_DEFAULT_ID = -1
    }
}