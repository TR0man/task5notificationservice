package com.example.task5notificationsmvp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.task5notificationsmvp.databinding.ActivityMainBinding
import com.example.utils.Constants.Companion.ACTION_CHANGE


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private var presenter = Presenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        checkIntent(intent)
    }

    private fun checkIntent(intent: Intent?) {
        if (intent?.extras != null) {
            if (intent.action.equals(ACTION_CHANGE)) {
                presenter.getLastNotificationInfo(intent)
            }
        } else {
            Log.d(Presenter.TAG, " [MainActivity] -> checkIntent FUNCTION -> intent = NO EXTRAS")
        }
    }

    // set button listeners
    private fun setListeners() {
        binding.btnMainStartNotification.setOnClickListener {
            presenter.startNotification()
        }

        binding.btnMainStopNotification.setOnClickListener {
            presenter.stopNotification()
        }
    }

    // create short message for user
    fun createToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }
}










