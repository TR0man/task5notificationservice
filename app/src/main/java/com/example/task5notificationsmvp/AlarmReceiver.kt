package com.example.task5notificationsmvp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.utils.Constants.Companion.ACTION_NEW_ALARM
import com.example.utils.Constants.Companion.ACTION_ONE_TIME
import com.example.utils.Constants.Companion.ACTION_ONE_TIME_ALARM
import com.example.utils.Constants.Companion.ACTION_REBOOT
import com.example.utils.Constants.Companion.ACTION_RESTART

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        if (intent.action.equals("android.intent.action.BOOT_COMPLETED")){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                context.startForegroundService(Intent(context, NotificationService::class.java).setAction(ACTION_REBOOT))
            } else {
            context.startService(Intent(context, NotificationService::class.java).setAction(ACTION_REBOOT))
            }
        }

        if(intent.action.equals(ACTION_ONE_TIME_ALARM)){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                context.startForegroundService(Intent(context, NotificationService::class.java).setAction(ACTION_ONE_TIME))
            } else {
                context.startService(Intent(context, NotificationService::class.java).setAction(ACTION_ONE_TIME))
            }
        }

        if (intent.action.equals(ACTION_NEW_ALARM)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                context.startForegroundService(Intent(context, NotificationService::class.java).setAction(ACTION_RESTART))
            } else {
                context.startService(Intent(context, NotificationService::class.java).setAction(ACTION_RESTART))
            }
        }
    }

}