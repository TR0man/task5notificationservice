package com.example.task5notificationsmvp

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.utils.Constants.Companion.ACTION_START
import com.example.utils.Constants.Companion.DEFAULT_NOTIFICATION_TIME
import com.example.utils.Constants.Companion.EXTRAS_DEFAULT_VALUE
import com.example.utils.Constants.Companion.MESSAGE_TEXT
import com.example.utils.Constants.Companion.TIME_MINUTES
import com.example.utils.Constants.Companion.TIME_SECONDS
import com.example.utils.Constants.Companion.WRONG_TIME_PARAMETER

class Presenter(private var view: MainActivity) {

    private val serviceIntent by lazy { Intent(view, NotificationService::class.java) }

    companion object {                                                                              // константы c 12-15 вынести в отдельный класс
        val TAG = Presenter::class.simpleName                                                       // убать My // писать так   - ОК
    }
//        Constants.CONSTANT_NAME               // вызывать через экземпляр класса, имя класса
//        import full name                      // или импортировать каждую константу по отдельности?


    // preparing data to start and start service
    fun startNotification() {                                                                       // 21-41 убрать вложенность         - ОК

        if (isServiceRunning()) {
            view.createToast(view.getString(R.string.main_message_notification_already_start))
            return
        }

        if (!checkIncomingData()) {
            return
        }

        serviceIntent.action = ACTION_START
        serviceIntent.putExtra(MESSAGE_TEXT, view.binding.etMainMessageText.text.toString())
        serviceIntent.putExtra(TIME_MINUTES, view.binding.etMainTimeMinutes.text.toString().toInt())

        if (view.binding.etMainTimeMinutes.text.toString() == WRONG_TIME_PARAMETER && view.binding.etMainTimeSeconds.text.toString() == WRONG_TIME_PARAMETER) {
            serviceIntent.putExtra(TIME_SECONDS, DEFAULT_NOTIFICATION_TIME)
            view.createToast(String.format(view.getString(R.string.default_time_message), DEFAULT_NOTIFICATION_TIME))
        } else {
            serviceIntent.putExtra(TIME_SECONDS, view.binding.etMainTimeSeconds.text.toString().toInt())
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {                 // ДОБАВЛЕНО СЕРВИСЫ 8.0
            view.startForegroundService(serviceIntent)
        } else {
            view.startService(serviceIntent)                                                        // проблема в разных версиях андроид (8.0 и выше) (ЧАСТИЧНО РЕШЕНО)
        }

        view.createToast(view.getString(R.string.main_message_notification_start))
    }

    // stop notification service
    fun stopNotification() {
        if (isServiceRunning()) {
            view.stopService(serviceIntent)
            view.createToast(view.getString(R.string.main_message_remove_notification))
        } else {
            view.createToast(view.getString(R.string.main_message_no_notification))
        }
    }

    // check incoming data from View activity
    private fun checkIncomingData(): Boolean {

        if (view.binding.etMainMessageText.text?.trim().isNullOrEmpty()) {
            view.createToast(view.getString(R.string.need_message))
            return false
        }

        if (view.binding.etMainTimeMinutes.text.isNullOrEmpty()) {
            view.createToast(view.getString(R.string.need_minutes))
            return false
        }

        if (view.binding.etMainTimeSeconds.text.isNullOrEmpty()) {
            view.createToast(view.getString(R.string.need_seconds))
            return false
        }
        return true
    }

    // display previously set reminder parameters
    fun getLastNotificationInfo(intent: Intent) {
        view.binding.etMainMessageText.setText(intent.getStringExtra(MESSAGE_TEXT))
        view.binding.etMainTimeMinutes.setText(intent.getIntExtra(TIME_MINUTES, EXTRAS_DEFAULT_VALUE).toString())
        view.binding.etMainTimeSeconds.setText(intent.getIntExtra(TIME_SECONDS, EXTRAS_DEFAULT_VALUE).toString())
        stopNotification()
    }

    private fun isServiceRunning(): Boolean {
        val activityManager = view.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        //get list of running services
        val runningService = activityManager.getRunningServices(Int.MAX_VALUE)                      // getRunningServices() >>> Deprecated in API level 26 ????

        val serviceName = NotificationService::class.qualifiedName

        for (service in runningService) {
            if (serviceName.equals(service.service.className)) {
                return true
            }
        }
        return false
    }

}