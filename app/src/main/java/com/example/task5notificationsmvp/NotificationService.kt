package com.example.task5notificationsmvp

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import com.example.utils.Constants
import com.example.utils.Constants.Companion.ACTION_CHANGE
import com.example.utils.Constants.Companion.ACTION_NEW_ALARM
import com.example.utils.Constants.Companion.ACTION_ONE_TIME
import com.example.utils.Constants.Companion.ACTION_ONE_TIME_ALARM
import com.example.utils.Constants.Companion.ACTION_REBOOT
import com.example.utils.Constants.Companion.ACTION_REPLY
import com.example.utils.Constants.Companion.ACTION_RESTART
import com.example.utils.Constants.Companion.ACTION_START
import com.example.utils.Constants.Companion.ACTION_STOP
import com.example.utils.Constants.Companion.CHANEL_ID
import com.example.utils.Constants.Companion.CHANEL_NAME
import com.example.utils.Constants.Companion.DEFAULT_FLAG_STATE
import com.example.utils.Constants.Companion.DEFAULT_NOTIFICATION_TIME
import com.example.utils.Constants.Companion.DEFAULT_REQUEST_CODE
import com.example.utils.Constants.Companion.EXTRAS_DEFAULT_VALUE
import com.example.utils.Constants.Companion.FOREGROUND_DEFAULT_ID
import com.example.utils.Constants.Companion.GROUP_ID
import com.example.utils.Constants.Companion.MESSAGE_TEXT
import com.example.utils.Constants.Companion.REPLY_ID
import com.example.utils.Constants.Companion.REPLY_NOTIFICATION_ID
import com.example.utils.Constants.Companion.REPLY_SLEEP_TIME
import com.example.utils.Constants.Companion.TIME_MINUTES
import com.example.utils.Constants.Companion.TIME_SECONDS
import com.example.utils.Constants.Companion.VIBRATE_PATTERN
import com.example.utils.Constants.Companion.ZERO
import java.util.*
import java.util.concurrent.TimeUnit

class NotificationService : Service() {

    var alarmText = ""
    var alarmMinute = 0
    var alarmSecond = 0
    var notificationId = 0

    private val alarmManager by lazy { getSystemService(ALARM_SERVICE) as AlarmManager }
    private lateinit var alarmPendingIntent: PendingIntent

    override fun onCreate() {
        super.onCreate()
        Log.d(Presenter.TAG, " [SERVICE] -> onCreate")
    }

    private fun fakeStartService() {
        if (Build.VERSION.SDK_INT >= 26) {      // снова маг число !!!  - android.os.Build.VERSION_CODES.O

            val chanel = NotificationChannel(CHANEL_ID, CHANEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            chanel.description = "Chanel for notification settings"
            chanel.vibrationPattern = longArrayOf(0, 500, 200, 500, 200, 500)
            chanel.enableVibration(true)
            chanel.shouldVibrate()

            val notificationManagerChanel = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManagerChanel.createNotificationChannel(chanel)

//            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)        // сокращенная запись

            val notification = NotificationCompat.Builder(this, CHANEL_ID)
                    .setContentTitle("")
                    .setContentText("").build()
            startForeground(FOREGROUND_DEFAULT_ID, notification)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        fakeStartService()

        Log.d(Presenter.TAG, " [SERVICE] -> onStartCommand")

        if (intent == null) {
            Log.d(Presenter.TAG, " [SERVICE] > [onStartCommand] -> INTENT NULL OR SERVICE STOPPED!")
            return START_REDELIVER_INTENT
        }

        when (intent.action) {
            ACTION_START -> {
                alarmText = intent.getStringExtra(MESSAGE_TEXT).toString()
                alarmMinute = intent.getIntExtra(TIME_MINUTES, EXTRAS_DEFAULT_VALUE)
                alarmSecond = intent.getIntExtra(TIME_SECONDS, EXTRAS_DEFAULT_VALUE)
                setAlarmReceiver(setCalendar())
            }
            ACTION_RESTART -> {
                createNotification()
                setAlarmReceiver(setCalendar())
            }
            ACTION_STOP -> stopSelf()
            ACTION_REPLY -> reply(intent)
            ACTION_REBOOT -> setOneTimeNotification()
            ACTION_ONE_TIME -> createNotification()
        }

        return START_REDELIVER_INTENT
    }
    // one time notification after reboot
    private fun setOneTimeNotification() {
        alarmText = "Service started after reboot!"
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.SECOND, DEFAULT_NOTIFICATION_TIME)

        alarmPendingIntent = PendingIntent.getBroadcast(this, DEFAULT_REQUEST_CODE,
                Intent(this, AlarmReceiver::class.java)
                        .setAction(ACTION_ONE_TIME_ALARM), PendingIntent.FLAG_CANCEL_CURRENT)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmPendingIntent)
    }

    // data processing on message change (receive info from Notification (RemoteInput))
    private fun reply(intent: Intent) {

        val result = RemoteInput.getResultsFromIntent(intent)
        alarmText = result.getCharSequence(REPLY_ID).toString()

        val replyNotification = NotificationCompat.Builder(this, CHANEL_ID)                 // убрать DEPRECATED  добавить chanel для андроид 8 и выше
                .setSmallIcon(R.drawable.ic_notifications_changes_done)
                .setContentText(getString(R.string.message_after_change_text))
                .build()

        val backNotification = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        backNotification.notify(REPLY_NOTIFICATION_ID, replyNotification)
        backNotification.cancel(GROUP_ID)
        TimeUnit.SECONDS.sleep(REPLY_SLEEP_TIME)
        backNotification.cancel(REPLY_NOTIFICATION_ID)
    }

    // set calendar
    private fun setCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, alarmMinute)
        calendar.add(Calendar.SECOND, alarmSecond)
        return calendar
    }

    // create alarm
    private fun setAlarmReceiver(calendar: Calendar) {
        val alarmIntent = Intent(this, AlarmReceiver::class.java)
        alarmIntent.action = ACTION_NEW_ALARM
        alarmPendingIntent = PendingIntent.getBroadcast(this, DEFAULT_REQUEST_CODE,
                alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmPendingIntent)
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d(Presenter.TAG, " [SERVICE] -> onBind")
        TODO("Return the communication channel to the service.")
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(Presenter.TAG, " [SERVICE] -> onUnbind")
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(Presenter.TAG, " [SERVICE] -> onDestroy")
        alarmManager.cancel(alarmPendingIntent)
        clearNotificationWindow()
    }

    // clear all notifications generated by service
    private fun clearNotificationWindow() {
        val clear = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (notificationId > ZERO) {
            for (id in ZERO..notificationId)
                clear.cancel(id)
        }
        clear.cancel(GROUP_ID)
    }

    private fun createNotification() {
        createNotificationGroup()

        val pendingStop = PendingIntent.getService(this, DEFAULT_REQUEST_CODE,
                Intent(this, NotificationService::class.java)
                        .setAction(ACTION_STOP), DEFAULT_FLAG_STATE)

        val pendingChange = PendingIntent.getActivity(this, DEFAULT_REQUEST_CODE,
                Intent(this, MainActivity::class.java)
                        .setAction(ACTION_CHANGE)
                        .putExtra(MESSAGE_TEXT, alarmText)
                        .putExtra(TIME_MINUTES, alarmMinute)
                        .putExtra(TIME_SECONDS, alarmSecond), PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationForm = NotificationCompat.Builder(this, CHANEL_ID)
            .setSmallIcon(R.drawable.ic_notification_small_icon)
            .setContentTitle(getString(R.string.notification_text))
            .setContentText(alarmText)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setVibrate(VIBRATE_PATTERN)
            .setContentIntent(pendingChange)
            .setStyle(NotificationCompat.BigTextStyle().bigText(alarmText))
            .addAction(createReply())
            .addAction(R.drawable.ic_notifications_edit, getString(R.string.button_edit_text), pendingChange)
            .addAction(R.drawable.ic_notifications_stop, getString(R.string.button_stop_text), pendingStop)
            .setAutoCancel(true)
            .setGroup(Constants.GROUP_KEY)
            .build()

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(++notificationId, notificationForm)
    }

    // create notifications group
    private fun createNotificationGroup() {
        val groupForm = NotificationCompat.Builder(this, CHANEL_ID)
            .setSmallIcon(R.drawable.ic_notification_small_icon)
            .setGroup(Constants.GROUP_KEY)
            .setGroupSummary(true)
            .build()
        val notificationManagerGroup = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManagerGroup.notify(GROUP_ID, groupForm)
    }

    // create reply for notification (setLabel sets a hint in input line)
    private fun createReply(): NotificationCompat.Action? {

        val remoteInput = androidx.core.app.RemoteInput.Builder(REPLY_ID).run {
            setLabel(getString(R.string.button_change_default_text))
            build()
        }
        // CREATE replyPendingIntent for Action
        val replyPending = PendingIntent.getService(this, DEFAULT_REQUEST_CODE,
                Intent(this, NotificationService::class.java)
                        .setAction(ACTION_REPLY), PendingIntent.FLAG_UPDATE_CURRENT)
        // CREATE action
        val notificationAction = NotificationCompat.Action.Builder(android.R.drawable.ic_menu_send, getString(R.string.button_change_text), replyPending)
            .addRemoteInput(remoteInput)
            .build()
        return notificationAction
    }
}
